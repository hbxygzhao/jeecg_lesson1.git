package Struts.Action;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Struts.Form.ActionForm;
import Struts.Form.FullForm;
import Struts.Form.XmlBean;

public class ActionServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public void doGet(HttpServletRequest request,HttpServletResponse response)
			throws IOException,ServletException
			{   
				//获得请求头
				String path= this.getPath(request.getServletPath());
			    Map<String,XmlBean> map=(Map<String,XmlBean>)this.getServletContext().getAttribute("struts");
			    XmlBean xml=map.get(path);
			    String formclass=xml.getFormCalss();
			    ActionForm form=FullForm.full(formclass,request);
			    String actiontype=xml.getActionClass();
			    Action action=null;
			    String url="";
			    try
			    {
			    	Class cla=Class.forName(actiontype);
			    	action=(Action)cla.newInstance();
			    	url=action.execute(request, response,form, xml.getActionforward());
			    	
			    }
			    
			    catch(Exception e)
			    {
			    	System.out.println("ERROR:控制器异常！");
			    }
			    
			    RequestDispatcher dis=request.getRequestDispatcher(url);
			    dis.forward(request, response);
			    
			    
			}
			
			public void doPost(HttpServletRequest request,HttpServletResponse response)
					throws IOException,ServletException
					{
						
						this.doGet(request, response);
					}
			
			
			public String getPath(String servletPath)
			{
				
				return servletPath.split("\\.")[0];
			}
}
