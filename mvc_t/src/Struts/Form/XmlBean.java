package Struts.Form;

import java.util.HashMap;
import java.util.Map;

public class XmlBean {

	
	public XmlBean()
	{
		
		
	}
	
	
	private String beanName="";
	private String path="";
	private String actionType="";
	private String actionClass="";
	private String formCalss="";
	Map<String,String> actionforward=new HashMap<String,String>();
	public String getBeanName() {
		return beanName;
	}
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public String getActionClass() {
		return actionClass;
	}
	public void setActionClass(String actionClass) {
		this.actionClass = actionClass;
	}
	public String getFormCalss() {
		return formCalss;
	}
	public void setFormCalss(String formCalss) {
		this.formCalss = formCalss;
	}
	public Map<String, String> getActionforward() {
		return actionforward;
	}
	public void setActionforward(Map<String, String> actionforward) {
		this.actionforward = actionforward;
	}
	public String toString()
	{
		
		return this.path+"==="+this.beanName+"==="+this.actionforward+"==="+this.actionClass;
	}
	
}
