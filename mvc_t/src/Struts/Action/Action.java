package Struts.Action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Struts.Form.ActionForm;

public interface Action {

	
	String execute(HttpServletRequest request,HttpServletResponse response,ActionForm form,Map<String,String>actionForm);
}
