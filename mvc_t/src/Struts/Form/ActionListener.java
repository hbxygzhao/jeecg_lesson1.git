package Struts.Form;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ActionListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("提示：系统已注销！");

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		   ServletContext servletcontext=arg0.getServletContext();
	       String xmlpath=servletcontext.getInitParameter("struts-config");
	       String tomcatpath=servletcontext.getRealPath("\\");
	       System.out.println("xmlpath+tomcatpath"+tomcatpath+xmlpath);
	       try {
			Map<String,XmlBean> map=Struts_Xml.ddd(tomcatpath+xmlpath);
			
			servletcontext.setAttribute("struts", map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       System.out.println("提示：系统已加载完成！");
			
	}

}
