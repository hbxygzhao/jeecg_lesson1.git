package Struts.Form;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

public class Struts_Xml {
	
	public Struts_Xml()
	{
		
		
	}

	
	public static Map<String, XmlBean> ddd(String rpath) throws Exception
	{    
		 SAXBuilder builder=new SAXBuilder();
		 Document  document=builder.build(new File(rpath)); 
		 Element root=document.getRootElement();
		 Map<String,XmlBean> rmap=new HashMap<String,XmlBean>();
		 
		 Element action=root.getChild("aciton-mapping");
		 List<Element> map=action.getChildren();
		 for(Element e:map)
		 {   XmlBean actiono=new XmlBean();
			 String name=e.getAttributeValue("name");	
			 actiono.setBeanName(name);
			 Element actionform=root.getChild("formbeans");
			 List<Element> form =actionform.getChildren();
			 for(Element ex:form)
				 
			 {
				if(name.equals(ex.getAttributeValue("name")))
				{
					String formCalss=ex.getAttributeValue("class");
					actiono.setFormCalss(formCalss);
					break;
				}
				 
			 }
			 
			 String path=e.getAttributeValue("path");
			 String type=e.getAttributeValue("type");
			 actiono.setPath(path);
			 //~~
			 actiono.setActionClass(type);		
			 List<Element> forward=e.getChildren();
			 Map<String,String> mapo=new HashMap<String,String>();
			 for(Element f:forward)
			 {   	 
				 String nam=f.getAttributeValue("name");
				 String value=f.getAttributeValue("value");
				 mapo.put(nam, value);
			 }
			 actiono.setActionforward(mapo);
			 rmap.put(path, actiono);
		 }
		 
		return rmap;
	}

}
