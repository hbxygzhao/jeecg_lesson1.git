package bussiness;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Struts.Action.Action;
import Struts.Form.ActionForm;
import service.MyServlet.decide.DecideSericeImpl;
import service.MyServlet.decide.DecideService;
import service.vo.MessVo;

public class BussAction implements Action {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, ActionForm form,
			Map<String, String> actionForm) {
		// TODO Auto-generated method stub
		String url="";
		DecideService service=new DecideSericeImpl();
		Map<String,MessVo> map=service.getVo(request.getParameter("name"));  
	   //泛型，必须 string，keySet所有key的集合，因为key是唯一的，显然set最合适
		Set<String> set=map.keySet(); 
		//迭代器循环
		for(Iterator<String> it=set.iterator();it.hasNext();)
		{
			url=it.next();
			request.setAttribute("messa", map.get(url));
		}
		return url;
	}

	
	
}
